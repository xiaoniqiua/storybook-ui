### 一、关于NPM
1. 对于dependencies，只要有用户下载你这个包，dependencies里面的包都会进行安装。
2. 对于devDependencies，用户下载你这个包不会安装这些包。
3. 对于peerDependencies，用户下载你这个包后如果用户没安装会进行报警，如果用户已有这个包但版本与你的不匹配会报警。用户下载你这个包不会自动安装peerDependencies。
