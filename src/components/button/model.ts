import { ReactNode, AnchorHTMLAttributes, ButtonHTMLAttributes } from 'react';

// READ:因为ts的字面量类型与string类型转换有bug，默认会先去匹配string类型而非字面量类型，并且，类型无法进行循环引用，所以需要单独写出来再定义上
// 按钮类型
type btnType = 'primary' | 'primaryOutline' | 'secondary' | 'secondaryOutline' | 'tertiary' | 'outline' | 'inversePrimary' | 'inverseSecondary' | 'inverseOutline';
type AppearancesObj = {
    [key in btnType]: btnType;
};

export const APPEARANCES: AppearancesObj = {
    primary: 'primary',
    primaryOutline: 'primaryOutline',
    secondary: 'secondary',
    secondaryOutline: 'secondaryOutline',
    tertiary: 'tertiary',
    outline: 'outline',
    inversePrimary: 'inversePrimary',
    inverseSecondary: 'inverseSecondary',
    inverseOutline: 'inverseOutline',
};

export type AppearancesTypes = keyof typeof APPEARANCES;

// 按钮大小
type sizeType = 'small' | 'medium';
type sizeObj = {
    [key in sizeType]: sizeType;
};
export const SIZES: sizeObj = {
    small: 'small',
    medium: 'medium',
};
export type SizesTypes = keyof typeof SIZES;

// 按钮入参定义
// READ: 自定义属性必须导出，必须按此格式写注释，这种写法会被dockgen插件捕获从而显示到属性页面上。
export interface CustormButtonProps {
    /** 是否禁用 */
    disabled?: boolean;
    /** 是否加载中 */
    isLoading?: boolean;
    /** 是否是a标签 */
    isLink?: boolean;
    /** 是否替换加载中文本 */
    loadingText?: ReactNode;
    /** 按钮大小 */
    size?: SizesTypes;
    /** 按钮类型 */
    appearance?: AppearancesTypes;
    /** 无效点击 */
    isUnclickable?: boolean;
}

export type ButtonProps = CustormButtonProps & AnchorHTMLAttributes<HTMLAnchorElement> & ButtonHTMLAttributes<HTMLButtonElement>;
